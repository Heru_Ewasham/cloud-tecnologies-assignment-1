#README#

This is the first assignment in Cloud Tecnologies, made by Yngve Hestem.

To test the project, go to http://mysterious-inlet-37024.herokuapp.com/projectinfo/v1/github.com/<owner>/<repo>, example: http://mysterious-inlet-37024.herokuapp.com/projectinfo/v1/github.com/git/git


PS: If you write a '/' after the repo-name (ie. "http://mysterious-inlet-37024.herokuapp.com/projectinfo/v1/github.com/git/git/") you will get an error that you don't have correct/too few/many arguments.
