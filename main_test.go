package main

import "encoding/json"
import "testing"
import "net/http/httptest"
import "net/http"

func Test_decodeMapToStringArray(t *testing.T) {
	var languageMap map[string]uint
	// Hard-code an example JSON
	response := "{\"Java\": 10762184,\"Scala\": 4832937,\"Python\": 625284,\"Shell\": 86668,\"Batchfile\": 27517,\"XSLT\": 7116,\"HTML\": 5443}"
	// Hard-code the "hopefully" returned array
	languageSrc := []string{"Java", "Scala", "Python", "Shell", "Batchfile", "XSLT", "HTML"}
	decodeErr := json.Unmarshal([]byte(response), &languageMap) // Decode the response
	languageArray := decodeMapToStringArray(languageMap)        // Check the function
	if decodeErr != nil {
		t.Error("Could not decode json. Error: ", decodeErr)
		return
	}
	if len(languageArray) != len(languageSrc) { // Check if they have equal length
		t.Error("Arrays has not equal length. Original array: ", languageSrc, ". Array via map: ", languageArray)
		return
	}
	// Don't do any more checks now (hopefully equal number will be equal in some way),
	// but could of course add a loop to check every value against each other)
}

func Test_copyStringArray(t *testing.T) {
	var newArray []string
	// The original array to copy
	srcArray := []string{"Hello", "Are", "You", "Java", "Friend", "or", "not"}
	newArray = copyStringArray(srcArray) // Copy array
	if len(newArray) != len(srcArray) {  // Check the length
		t.Error("The array's is not equal in length when copied")
		return
	}
	// Could of course also here do more checks.
}

func Test_getAPIResponse(t *testing.T) {
	response, httpErr := getAPIResponse("apache", "kafka", "") // Check the apache/kafka repo
	defer response.Body.Close()
	if httpErr != nil {
		t.Error("Couldn't connect to Github API (repo \"apache/kafka\"). Error: ", httpErr)
		return
	}
	tempProject := ProjectInfoFromGithub{}
	decodeErr := json.NewDecoder(response.Body).Decode(&tempProject)
	if decodeErr != nil {
		t.Error("Error while decoding response from Github API (repo \"apache/kafka\"). Error: ", decodeErr)
		return
	}
	if tempProject.Owner.Login != "apache" { // Check if owner is "apache" since the owner will not change (but the rest could change)
		t.Error("The owner of the repo \"apache/kafka\" is not \"apache\".")
		return
	}
}

// Testing the whole program/handler
func Test_handler(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(handler))
	defer ts.Close()

	// Check with the apache/kafka url
	req, err := http.Get(ts.URL + "/projectinfo/v1/github.com/apache/kafka")
	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
		return
	}
	// Decode the answered JSON and then check that owner is "apache"
	project := Project{}
	decodeErr := json.NewDecoder(req.Body).Decode(&project)
	defer req.Body.Close()
	if decodeErr != nil {
		t.Error("Decoding the JSON from handler didn't work. ", decodeErr)
		return
	}
	if project.Owner != "apache" {
		t.Error("Owner of apache/kafka should be apache, not " + project.Owner)
		return
	}
}
