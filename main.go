package main

import "fmt"
import "encoding/json"
import "net/http"
import "strings"
import "os"

// Project - This is the struct which will be outputted to the user as JSON.
type Project struct {
	Project   string   `json:"project"`
	Owner     string   `json:"owner"`
	Committer string   `json:"committer"`
	Commits   int      `json:"commits"`
	Language  []string `json:"language"`
}

//----------------------------------------------------------------

// ProjectInfoFromGithub - This is a struct that handle the result gotten from git.
// The structure of this struct is based on the struct to Kent Holt.
type ProjectInfoFromGithub struct {
	Name  string `json:"full_name"`
	Owner struct {
		Login string `json:"login"`
	} `json:"Owner"`
	Lang       []string
	TopContrib []struct {
		Name    string `json:"login"`
		Commits int    `json:"contributions"`
	}
}

//-------------------------------------------------------------------

// decodeMapToStringArray - Based on: https://stackoverflow.com/questions/21362950/golang-getting-a-slice-of-keys-from-a-map and Kent.
func decodeMapToStringArray(langMap map[string]uint) []string {
	array := make([]string, 0, len(langMap))
	for k := range langMap {
		array = append(array, k)
	}
	return array
}

//------------------------------

// copyStringArray - Function to copy a bigger array into a smaller one (I can only find a function that copy a smaller array into a bigger one, so therefore I make my own)
func copyStringArray(srcArr []string) []string {
	array := make([]string, 0, len(srcArr))
	for k := range srcArr {
		array = append(array, srcArr[k])
	}
	return array
}

// getAPIRespone - Function to call http.get so we don't need to write URL in every call:
func getAPIResponse(owner string, repo string, apiPart string) (*http.Response, error) {
	response, httpErr := http.Get("https://api.github.com/repos/" + owner + "/" + repo + apiPart)
	return response, httpErr
}

// reformat - Function to copy over from ProjectInfoFromGithub to Project:
func reformat(tempProject *ProjectInfoFromGithub) Project {
	project := Project{}
	nameParts := strings.Split(tempProject.Name, "/")    // Parts of name (you want)
	project.Project = nameParts[1]                       // Copy project name (ie. 'kafka' instead of 'apache/kafka')
	project.Owner = tempProject.Owner.Login              // Copy username of owner to correct struct.
	project.Committer = tempProject.TopContrib[0].Name   // Copy name and Commits of person with
	project.Commits = tempProject.TopContrib[0].Commits  // most commits into correct struct.
	project.Language = copyStringArray(tempProject.Lang) // Copy languages array to correct struct.
	return project
}

func handler(w http.ResponseWriter, r *http.Request) {
	http.Header.Add(w.Header(), "content-type", "application/json")
	parts := strings.Split(r.URL.Path, "/") // Parts of response
	if len(parts) != 6 || parts[3] != "github.com" {
		http.Error(w, "Bad request: You didn't give us enough/correct arguments", http.StatusBadRequest)
		return
	}

	// Get general information of the repo
	tempProject := ProjectInfoFromGithub{}
	response, httpErr := getAPIResponse(parts[4], parts[5], "")
	if httpErr != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	decodeErr := json.NewDecoder(response.Body).Decode(&tempProject)
	defer response.Body.Close()
	if decodeErr != nil {
		http.Error(w, "Bad request: Decoding didn't work.", http.StatusBadRequest)
		return
	}
	defer response.Body.Close()

	// Get languages information
	var languageMap map[string]uint
	response, httpErr = getAPIResponse(parts[4], parts[5], "/languages")

	if httpErr != nil {
		http.Error(w, "Bad request.", http.StatusBadRequest)
		return
	}
	decodeErr = json.NewDecoder(response.Body).Decode(&languageMap)

	if decodeErr != nil {
		http.Error(w, "Bad request: Decoding didn't work.", http.StatusBadRequest)
		return
	}
	tempProject.Lang = decodeMapToStringArray(languageMap) // Parse languages from the map to the rest of the struct

	// Get information about contributors:
	response, httpErr = getAPIResponse(parts[4], parts[5], "/contributors")
	if httpErr != nil {
		http.Error(w, "Bad request.", http.StatusBadRequest)
		return
	}
	decodeErr = json.NewDecoder(response.Body).Decode(&tempProject.TopContrib)
	defer response.Body.Close()
	if decodeErr != nil {
		http.Error(w, "Bad request: Decoding didn't work.", http.StatusBadRequest)
		return
	}

	project := reformat(&tempProject) // Copy tempProject in the "better readable" Project-struct (better JSON)

	json.NewEncoder(w).Encode(&project) // Write the "better readable" Prosject-struct to user
}

func main() {
	http.HandleFunc("/projectinfo/v1/", handler)
	fmt.Println("Listen on Port:" + os.Getenv("PORT"))
	err := http.ListenAndServe(":"+os.Getenv("PORT"), nil)
	if err != nil {
		panic(err)
	}
}
